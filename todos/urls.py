from django.urls import path
from . import views

urlpatterns = [
    path("", views.todo_list, name="todo_list_list"),
    path("<int:id>/", views.todo_item, name="todo_list_detail"),
    path("create/", views.create_list, name="todo_list_create"),
    path("<int:id>/edit/", views.edit_list, name="todo_list_update"),
    path("<int:id>/delete/", views.delete_list, name="todo_list_delete"),
    path("items/create/", views.create_item, name="todo_item_create"),
    path("items/<int:id>/edit/", views.edit_item, name="todo_item_update"),
]
