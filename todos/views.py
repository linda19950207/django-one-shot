from django.shortcuts import (
    render,
    get_object_or_404,
    redirect,
)
from todos.models import TodoList, TodoItem
from todos.form import CreateListForm, CreateItemForm


# Create your views here.
def todo_list(request):
    lists = TodoList.objects.all()
    context = {
        "todolist": lists,
    }
    return render(request, "todos/list.html", context)


def todo_item(request, id):
    item = get_object_or_404(TodoList, id=id)
    context = {
        "todoitem": item,
    }
    return render(request, "todos/item.html", context)


def create_list(request):
    if request.method == "POST":
        form = CreateListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = CreateListForm
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_list(request, id):
    list_name = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = CreateListForm(request.POST, instance=list_name)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = CreateListForm(instance=list_name)
    context = {
        "list_name": list_name,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    list_name = TodoList.objects.get(id=id)
    if request.method == "POST":
        list_name.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = CreateItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = CreateItemForm
    context = {
        "form": form,
    }
    return render(request, "todos/createitem.html", context)


def edit_item(request, id):
    items_name = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = CreateItemForm(request.POST, instance=items_name)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = CreateItemForm(instance=items_name)
    context = {
        "item_name": items_name,
        "form": form,
    }
    return render(request, "todos/edititem.html", context)
