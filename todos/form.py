from todos.models import TodoList, TodoItem
from django.forms import ModelForm


class CreateListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class CreateItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
